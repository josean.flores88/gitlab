import { __, s__ } from '~/locale';

// i18n
export const USAGE_BY_MONTH = s__('UsageQuota|CI minutes usage by month');
export const USAGE_BY_PROJECT = s__('UsageQuota|CI minutes usage by project');
export const X_AXIS_MONTH_LABEL = __('Month');
export const X_AXIS_PROJECT_LABEL = __('Projects');
export const Y_AXIS_LABEL = __('Minutes');
export const NO_CI_MINUTES_MSG = s__('UsageQuota|No CI minutes usage data available.');

export const X_AXIS_CATEGORY = 'category';

export const MONTHS = {
  january: __('January'),
  february: __('February'),
  march: __('March'),
  april: __('April'),
  may: __('May'),
  june: __('June'),
  july: __('July'),
  august: __('August'),
  september: __('September'),
  october: __('October'),
  november: __('November'),
  december: __('December'),
};
